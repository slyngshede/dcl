# Todo

## Nested Builds

Outer container needs host networking, otherwise setting up the bridge
in the inner container, fails for reasons that are not well understood!!
used a trixie container, as bookworm's podman was busted on install!?

    $ sudo buildah build --build-arg DEBIAN_CODENAME=trixie --file Dockerfile.nested-build --tag nest
    $ sudo podman run --network host --privileged --rm -it -v ~/src/wmf/dcl:/mnt:ro -w /mnt/dockerfiles -v ~/src/wmf/puppet:/puppet:ro -v ~/src/wmf/labs-private:/labs:ro nest -- PUPPET_SRC=/puppet LABS_PRIVATE_SRC=/labs

This successfully builds the images, but they are not persisted, since
they are placed in the storage of the container.

- [ ] How should images be expoerted, bind mount the podman socket?
