package main

import (
	"fmt"
	"os"

	"gitlab.wikimedia.org/jhathaway/dcl/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		os.Exit(1)
	}
}
