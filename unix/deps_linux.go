package unix

import (
	"errors"
	"fmt"
	"os/exec"
)

func CheckDeps() error {
	deps := []string{
		"exportfs",
		"ip",
		"kubectl",
		"minikube",
		"podman",
		"resolvectl",
		"sudo",
	}
	for _, dep := range deps {
		_, err := exec.LookPath(dep)
		if err != nil {
			if errors.Is(err, exec.ErrNotFound) {
				return fmt.Errorf("Missing binary, please install, '%s', try `$ command-not-found exportfs` to find the appropriate package", dep)
			} else {
				return err
			}
		}
	}
	return nil
}
