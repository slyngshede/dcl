package unix

import (
	"encoding/json"
	"fmt"
	"net/netip"
)

// Get route info for an ip
func GetIpRoute(ip netip.Addr) (route IpRoute, err error) {
	ipRouteOut, err := ExecShStdout("ip", "-json", "route", "get", ip.String())
	if err != nil {
		return route, err
	}
	var routes []IpRoute
	err = json.Unmarshal([]byte(ipRouteOut), &routes)
	if err != nil {
		return route, err
	}
	if len(routes) == 0 {
		return IpRoute{}, nil
	} else if len(routes) == 1 {
		return routes[0], nil
	} else {
		return route, fmt.Errorf("Too many routes! %v", routes)
	}
}

func ShowRoute(cidr netip.Prefix) (route Route, err error) {
	ipRouteOut, err := ExecShStdout("ip", "-json", "ro", "show", cidr.String())
	if err != nil {
		return route, err
	}
	var routes []Route
	err = json.Unmarshal([]byte(ipRouteOut), &routes)
	if err != nil {
		return route, err
	}
	if len(routes) == 0 {
		return Route{}, nil
	} else if len(routes) == 1 {
		return routes[0], nil
	} else {
		return route, fmt.Errorf("Too many routes! %v", routes)
	}
}

func DelIpRoute(cidr netip.Prefix, gateway netip.Addr) error {
	err := ExecSh("sudo", "ip", "route", "del", cidr.String(), "via", gateway.String())
	if err != nil {
		return err
	}
	return nil
}

func AddIpRoute(cidr netip.Prefix, gateway netip.Addr) error {
	err := ExecSh("sudo", "ip", "route", "add", cidr.String(), "via", gateway.String())
	if err != nil {
		return err
	}
	return nil
}
