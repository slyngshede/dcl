package unix

import (
	"fmt"
	"net/netip"
	"time"
)

func formatExport(path string, cidr netip.Prefix) string {
	return fmt.Sprintf("%s -ro -mapall=nobody -network %s -mask %s", path, cidr.Addr(), netmask(cidr.Bits()))
}

// adapted from, https://cs.opensource.google/go/go/+/refs/tags/go1.21.3:src/net/ip.go;l=78
func netmask(ones int) (mask string) {
	n := uint(ones)
	for i := 0; i < 4; i++ {
		if n >= 8 {
			mask += fmt.Sprintf("%d", 0xff)
			n -= 8
		} else {
			mask += fmt.Sprintf("%d", ^byte(0xff>>n))
			n = 0
		}
		if i < 3 {
			mask += "."
		}
	}
	return mask
}

func exportFs() error {
	var err error
	err = ExecSh("sudo", "nfsd", "enable")
	if err != nil {
		return err
	}
	err = ExecSh("sudo", "nfsd", "start")
	if err != nil {
		return err
	}
	// equivalent to linux's exportfs -r
	err = ExecSh("sudo", "nfsd", "update")
	if err != nil {
		return err
	}
	// HACK: to ensure volumes are ready to serve
	// should monitor nfsd status probably
	time.Sleep(2 * time.Second)
	return nil
}
