package unix

func SplitDns(ip string, dev string, searchDomains []string) error {
	var err error
	addSearchArgs := append([]string{"resolvectl", "domain", dev}, searchDomains...)
	err = ExecSh("sudo", addSearchArgs...)
	if err != nil {
		return err
	}
	err = ExecSh("sudo", "resolvectl", "default-route", dev, "false")
	if err != nil {
		return err
	}
	err = ExecSh("sudo", "resolvectl", "dns", dev, ip)
	if err != nil {
		return err
	}
	return nil
}
