package unix

import (
	"bufio"
	"bytes"
	"embed"
	"errors"
	"fmt"
	"math/bits"
	"net/netip"
	"os"
	"os/exec"
	"os/user"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"text/template"
)

//go:embed templates
var efs embed.FS

type IpRoute struct {
	Dst     netip.Addr `json:"dst"`
	Gateway netip.Addr `json:"gateway"`
	Dev     string     `json:"dev"`
	Prefsrc netip.Addr `json:"prefsrc"`
}

type Route struct {
	Dst     netip.Prefix `json:"dst"`
	Gateway netip.Addr   `json:"gateway"`
	Dev     string       `json:"dev"`
	Prefsrc netip.Addr   `json:"prefsrc"`
}

func parseDarwinIpRoute(out string) (route IpRoute, err error) {
	reRouteTo := regexp.MustCompile(`^ *route to: (?P<Ip>.*)`)
	reDev := regexp.MustCompile(`^ *interface: (?P<DevName>.*)`)
	reGateway := regexp.MustCompile(`^ *gateway: (?P<Ip>.*)`)
	scanner := bufio.NewScanner(bytes.NewBufferString(out))
	for scanner.Scan() {
		line := scanner.Text()
		if m := reRouteTo.FindStringSubmatch(line); m != nil {
			dst, err := netip.ParseAddr(m[reRouteTo.SubexpIndex("Ip")])
			if err != nil {
				return route, err
			}
			route.Dst = dst
		} else if m := reDev.FindStringSubmatch(line); m != nil {
			route.Dev = m[reDev.SubexpIndex("DevName")]
		} else if m := reGateway.FindStringSubmatch(line); m != nil {
			gateway, err := netip.ParseAddr(m[reGateway.SubexpIndex("Ip")])
			if err != nil {
				return route, err
			}
			route.Gateway = gateway
		}
	}
	return route, nil
}

func parseDarwinRoute(out string) (route Route, err error) {
	reRouteTo := regexp.MustCompile(`^ *route to: (?P<RouteToNet>.*)`)
	reDst := regexp.MustCompile(`^ *destination: (?P<DstNet>.*)`)
	reMask := regexp.MustCompile(`^ *mask: (?P<Netmask>.*)`)
	reGateway := regexp.MustCompile(`^ *gateway: (?P<Gateway>.*)`)
	reDev := regexp.MustCompile(`^ *interface: (?P<DevName>.*)`)
	scanner := bufio.NewScanner(bytes.NewBufferString(out))
	var routeTo, dst, netmask, gateway, dev string
	for scanner.Scan() {
		line := scanner.Text()
		if m := reRouteTo.FindStringSubmatch(line); m != nil {
			routeTo = m[reRouteTo.SubexpIndex("RouteToNet")]
		} else if m := reDst.FindStringSubmatch(line); m != nil {
			dst = m[reDst.SubexpIndex("DstNet")]
		} else if m := reMask.FindStringSubmatch(line); m != nil {
			netmask = m[reMask.SubexpIndex("Netmask")]
		} else if m := reGateway.FindStringSubmatch(line); m != nil {
			gateway = m[reGateway.SubexpIndex("Gateway")]
		} else if m := reDev.FindStringSubmatch(line); m != nil {
			dev = m[reDev.SubexpIndex("DevName")]
		}
	}
	// HACK to determine if a route exists in the routing table, not sure if
	// this always works, I could parse netstat -r, but that looks pretty tough
	if routeTo != dst {
		return route, nil
	}
	cidr, err := parseNetmask(netmask)
	if err != nil {
		return route, err
	}

	route.Dst, err = netip.ParsePrefix(dst + "/" + strconv.Itoa(cidr))
	if err != nil {
		return route, err
	}
	route.Gateway, err = netip.ParseAddr(gateway)
	if err != nil {
		return route, err
	}
	route.Dev = dev
	return route, nil
}

// Adapted from https://stackoverflow.com/a/57701071
func parseNetmask(netmask string) (int, error) {
	var mask uint32
	parseErr := fmt.Errorf("Unable to parse netmask '%s'", netmask)
	quad := strings.Split(netmask, ".")
	if len(quad) != 4 {
		return 0, parseErr
	}
	for idx, dotpart := range quad {
		part, err := strconv.Atoi(dotpart)
		if err != nil {
			return 0, parseErr
		}
		mask = mask | uint32(part)<<uint32(24-idx*8)
	}
	return bits.OnesCount32(mask), nil
}

// Shell style exec with stdout and stderr inline
func ExecSh(name string, arg ...string) error {
	cmd := exec.Command(name, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	// print our args, make style
	fmt.Println(cmd)
	return cmd.Run()
}

// Shell style capture of stdout and trailing newline capture
func ExecShStdout(name string, arg ...string) (string, error) {
	cmd := exec.Command(name, arg...)
	cmd.Stderr = os.Stderr
	bytesStdout, err := cmd.Output()
	return strings.TrimSuffix(string(bytesStdout), "\n"), err
}

func ExecShStdin(stdin string, name string, arg ...string) error {
	cmd := exec.Command(name, arg...)
	cmd.Stdin = strings.NewReader(stdin)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	// print our args, make style
	fmt.Printf("%v <<EOF\n", cmd)
	fmt.Printf("%v", stdin)
	fmt.Printf("EOF\n")
	return cmd.Run()
}

func ExportNfs(codePaths map[string]string, cidr netip.Prefix) error {
	var err error
	alreadyExported := true
	conf := "/etc/exports"

	codeExports := make(map[string]bool)
	for _, path := range codePaths {
		codeExports[formatExport(path, cidr)] = false
	}

	var newExports strings.Builder
	if exports, err := os.Open(conf); err == nil {
		scanner := bufio.NewScanner(exports)
		for scanner.Scan() {
			export := scanner.Text()
			if _, ok := codeExports[export]; ok {
				codeExports[export] = true
			}
			newExports.WriteString(export + "\n")
		}
		err = exports.Close()
		if err != nil {
			return err
		}
		for export, exported := range codeExports {
			if !exported {
				newExports.WriteString(export + "\n")
				alreadyExported = false
			}
		}
		if alreadyExported {
			return nil
		}
	} else if errors.Is(err, os.ErrNotExist) {
		for export := range codeExports {
			newExports.WriteString(export + "\n")
		}
	} else {
		return err
	}

	err = CreateOrUpdateConf(conf, newExports.String(), nil)
	if err != nil {
		return err
	}
	err = exportFs()
	if err != nil {
		return err
	}
	return nil
}

func GetMinikubeIp(profile string) (ip netip.Addr, err error) {
	ipOut, err := ExecShStdout(
		"minikube",
		"--profile="+profile,
		"ip",
	)
	if err != nil {
		return ip, err
	}
	ip, err = netip.ParseAddr(ipOut)
	if err != nil {
		return ip, err
	}
	return ip, nil
}

func CreateContainersConf() error {
	desiredConf := `# We run out of pids quickly, since we are runing a full OS,
# so set to pids to unlimited
[containers]
pids_limit=0
`
	conf_d := "/etc/containers/containers.conf.d"

	if _, err := os.Stat(conf_d); errors.Is(err, os.ErrNotExist) {
		err = ExecSh("sudo", "mkdir", conf_d)
		if err != nil {
			return err
		}
	}

	conf := conf_d + "/dcl.conf"
	return CreateOrUpdateConf(conf, desiredConf, nil)
}

func CreateOrUpdateConf(conf string, desiredConf string, confirm func() bool) error {
	update := false
	if _, err := os.Stat(conf); err == nil {
		oldConf, err := os.ReadFile(conf)
		if err != nil {
			return err
		}
		if string(oldConf) != desiredConf {
			update = true
		}
	} else if errors.Is(err, os.ErrNotExist) {
		update = true
	} else {
		return err
	}
	if update == false {
		return nil
	}

	if confirm != nil {
		if !confirm() {
			return fmt.Errorf("Update of conf file %s denied", conf)
		}
	}

	f, err := os.CreateTemp("", "dclNewConf")
	if err != nil {
		return err
	}
	if _, err := f.Write([]byte(desiredConf)); err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	err = ExecSh("sudo", "cp", f.Name(), conf)
	if err != nil {
		return err
	}
	err = ExecSh("sudo", "chmod", "0644", conf)
	if err != nil {
		return err
	}
	err = os.Remove(f.Name())
	if err != nil {
		return err
	}
	return nil
}

func CreatePodmanSudoersConf() error {
	var err error
	var desiredConf strings.Builder

	sudoersMinikube, err := template.ParseFS(efs, "templates/minikube_podman.tmpl")
	if err != nil {
		return err
	}
	curUser, err := user.Current()
	if err != nil {
		return err
	}
	data := struct {
		User string
	}{
		curUser.Username,
	}
	err = sudoersMinikube.Execute(&desiredConf, data)
	if err != nil {
		return err
	}

	conf := "/etc/sudoers.d/minikube-podman"

	return CreateOrUpdateConf(
		conf,
		desiredConf.String(),
		func() bool { return Confirm("allow podman commands as root, needed by minikube") },
	)
}

// Adapted from, https://gist.github.com/C123R/0ba4bcad0ef10eb2f2102899c7e650dd
func Confirm(op string) bool {

	var input string

	fmt.Printf("Do you want to %s? (y|n): ", op)
	_, err := fmt.Scanln(&input)
	if err != nil {
		return false
	}
	input = strings.ToLower(input)

	if input == "y" || input == "yes" {
		return true
	}
	return false
}

func CreateDclSudoersConf() error {
	var err error
	var desiredConf strings.Builder
	sudoersDclName := fmt.Sprintf("sudoers_%s_dcl.tmpl", runtime.GOOS)
	sudoersDcl, err := template.ParseFS(efs, "templates/"+sudoersDclName)
	if err != nil {
		return err
	}

	curUser, err := user.Current()
	if err != nil {
		return err
	}
	data := struct {
		User string
	}{
		curUser.Username,
	}
	err = sudoersDcl.Execute(&desiredConf, data)
	if err != nil {
		return err
	}

	conf := "/etc/sudoers.d/dcl"

	var msg string
	switch runtime.GOOS {
	case "linux":
		msg = "allow ip, resolvectl & exportfs commands as root"
	case "darwin":
		msg = "allow scutil, nfsd & route commands as root"
	}
	return CreateOrUpdateConf(
		conf,
		desiredConf.String(),
		func() bool { return Confirm(msg) },
	)
}
