package unix

import (
	"errors"
	"net"
	"net/netip"
)

// Get route info for an ip
func GetIpRoute(ip netip.Addr) (route IpRoute, err error) {
	routeOut, err := ExecShStdout("route", "-n", "get", ip.String())
	if err != nil {
		return route, err
	}
	// stolen from https://github.com/brona/iproute2mac
	route, err = parseDarwinIpRoute(routeOut)
	if err != nil {
		return route, err
	}
	conn, err := net.Dial("udp", route.Dst.String()+":1024")
	if err != nil {
		return route, err
	}
	switch addr := conn.LocalAddr().(type) {
	case *net.UDPAddr:
		prefIp, err := netip2Addr(addr.IP)
		if err != nil {
			return route, err
		}
		route.Prefsrc = prefIp
	}
	return route, nil
}

func ShowRoute(cidr netip.Prefix) (route Route, err error) {
	routeOut, err := ExecShStdout("route", "-n", "get", cidr.String())
	if err != nil {
		return route, err
	}
	route, err = parseDarwinRoute(routeOut)
	if err != nil {
		return route, err
	}
	return route, nil
}

// Convert a net.IP to netip.Addr
// stolen from, https://djosephsen.github.io//posts/ipnet/
func netip2Addr(ip net.IP) (netip.Addr, error) {
	if addr, ok := netip.AddrFromSlice(ip); ok {
		return addr, nil
	}
	return netip.Addr{}, errors.New("Invalid IP")
}

func DelIpRoute(cidr netip.Prefix, gateway netip.Addr) error {
	err := ExecSh("sudo", "route", "-n", "delete", "-net", cidr.String(), gateway.String())
	if err != nil {
		return err
	}
	return nil
}

func AddIpRoute(cidr netip.Prefix, gateway netip.Addr) error {
	err := ExecSh("sudo", "route", "-n", "add", "-net", cidr.String(), gateway.String())
	if err != nil {
		return err
	}
	return nil
}
