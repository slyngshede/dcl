package cmd

import (
	"github.com/spf13/cobra"
)

func delCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:               "del",
		PersistentPreRunE: preRunLoadCfg,
		Short:             "delete data center lab nodes",
	}
	cmd.AddCommand(delSetCmd())
	cmd.AddCommand(delUnionCmd())
	cmd.AddCommand(delNodesCmd())
	return cmd
}
