package cmd

import (
	"github.com/spf13/cobra"
)

var Prune bool

func addCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:               "add",
		PersistentPreRunE: preRunLoadCfg,
		Short:             "add nodes to your data center lab",
	}
	cmd.PersistentFlags().BoolVarP(&Prune, "prune", "p", false, "prune other running nodes")
	cmd.AddCommand(addSetCmd())
	cmd.AddCommand(addUnionCmd())
	cmd.AddCommand(addNodesCmd())
	return cmd
}
