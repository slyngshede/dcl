package cmd

import (
	"fmt"
	"golang.org/x/exp/slices"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
)

func addSetCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:               "set",
		Short:             "add node set from config",
		Args:              cobra.ExactArgs(1),
		RunE:              addSet,
		ValidArgsFunction: addSetComp,
	}
	return cmd
}

func addSetComp(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	if len(args) != 0 {
		return nil, cobra.ShellCompDirectiveNoFileComp
	}
	err := specCfgLoad()
	if err != nil {
		return nil, cobra.ShellCompDirectiveNoFileComp
	}
	var desc string
	var compReply []string
	for name, set := range Cfg.NodeSets {
		if set.Desc == "" {
			desc = strings.Join(set.Hosts, "")
		} else {
			desc = set.Desc
		}
		compReply = append(compReply, fmt.Sprintf("%s\t%s", name, desc))
	}
	slices.Sort(compReply)
	return compReply, cobra.ShellCompDirectiveNoFileComp
}

func addSet(cmd *cobra.Command, args []string) error {
	var err error
	nodeSetName := args[0]
	nodeSet, err := cfg.LoadNodeSet(nodeSetName, Cfg)
	if err != nil {
		return err
	}
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	return kubenode.Reconcile(nodeSet, Prune, restclient, clientset, Cfg)
}
