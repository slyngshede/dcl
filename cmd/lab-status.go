package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
)

func labStatusCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "status",
		Short: "query lab status",
		Args:  cobra.NoArgs,
		RunE:  labStatus,
	}
	return cmd
}

// TODO: This is a quick and dirty status command, it could be improved with
// more readable output that doesn't expose the underlying implementation.
func labStatus(cmd *cobra.Command, args []string) error {
	var err error
	err = unix.ExecSh("minikube", "--profile="+Cfg.DataCenter.Name, "status")
	if err != nil {
		return err
	}
	err = unix.ExecSh("kubectl", "get", "pods")
	if err != nil {
		return err
	}
	return nil
}
