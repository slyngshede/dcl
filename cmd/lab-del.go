package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
	"runtime"
)

func labDelCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:     "del",
		Short:   "delete your data center lab",
		Args:    cobra.NoArgs,
		RunE:    labDel,
		Aliases: []string{"delete"},
	}
	return cmd
}

var scutilDnsDel = `open
remove State:/Network/Service/dcl/DNS
close
`

func labDel(cmd *cobra.Command, args []string) error {
	var err error
	minikubeArgs := []string{
		"--profile=" + Cfg.DataCenter.Name,
		"delete",
	}
	err = unix.ExecSh("minikube", minikubeArgs...)
	if err != nil {
		return err
	}

	switch runtime.GOOS {
	case "darwin":
		err = unix.ExecShStdin(scutilDnsDel, "sudo", "scutil")
		if err != nil {
			return err
		}
	}

	return nil
}
