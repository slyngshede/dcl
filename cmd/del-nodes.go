package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
)

func delNodesCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "nodes",
		Short: "delete ad hoc running nodes",
		Args:  cobra.MinimumNArgs(1),
		RunE:  delNodes,
	}
	return cmd
}

func delNodes(cmd *cobra.Command, argNodes []string) error {
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	nodes, err := cfg.ValidateCfgNodes(argNodes, "")
	if err != nil {
		return err
	}
	return kubenode.Del(nodes, restclient, clientset, Cfg)
}
