package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
)

func delUnionCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "union",
		Short: "delete a running node union",
		Args:  cobra.ExactArgs(1),
		RunE:  delUnion,
		// TODO: ideally this would complete only running unions
		ValidArgsFunction: addUnionComp,
	}
	return cmd
}

func delUnion(cmd *cobra.Command, args []string) error {
	var err error
	nodeUnionSetName := args[0]
	nodeSet, err := cfg.LoadUnionSet(nodeUnionSetName, Cfg)
	if err != nil {
		return err
	}
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	return kubenode.Del(nodeSet, restclient, clientset, Cfg)
}
