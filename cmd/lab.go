package cmd

import (
	"github.com/spf13/cobra"
)

func labCmd() *cobra.Command {
	lab := &cobra.Command{
		Use:               "lab",
		PersistentPreRunE: preRunLoadCfg,
		Short:             "manage your lab",
	}
	lab.AddCommand(labDelCmd())
	lab.AddCommand(labStartCmd())
	lab.AddCommand(labStatusCmd())
	lab.AddCommand(labStopCmd())
	return lab
}
