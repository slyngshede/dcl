package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
)

func listCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "list",
		Short: "list nodes in lab",
		Args:  cobra.NoArgs,
		RunE:  listNodes,
	}
	return cmd
}

// TODO: This is a quick and dirty list command, it could be improved with more
// readable output that doesn't expose the underlying implementation.
func listNodes(cmd *cobra.Command, args []string) error {
	var err error
	err = unix.ExecSh("kubectl", "get", "pods")
	if err != nil {
		return err
	}
	return nil
}
