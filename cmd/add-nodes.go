package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
)

var Codename string

func addNodesCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "nodes",
		Short: "add adhoc nodes",
		Args:  cobra.MinimumNArgs(1),
		RunE:  addNodes,
	}
	cmd.PersistentFlags().StringVarP(&Codename, "codename", "c", "", "Debian version codename, e.g. buzz")
	cmd.MarkPersistentFlagRequired("codename")
	return cmd
}

func addNodes(cmd *cobra.Command, argNodes []string) error {
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	nodes, err := cfg.ValidateCfgNodes(argNodes, Codename)
	if err != nil {
		return err
	}
	return kubenode.Reconcile(nodes, Prune, restclient, clientset, Cfg)
}
