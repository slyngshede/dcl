package kubenode

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"text/template"
	"time"

	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
	"k8s.io/apimachinery/pkg/util/sets"
)

import _ "embed"

// k8s imports
import (
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	watch "k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
)

//go:embed coredns.cfg.tmpl
var coreDnsCfgFile string
var coreDnsCfg = template.Must(template.New("coreDnsCfg").Parse(coreDnsCfgFile))

func Reconcile(nodes []cfg.Node, prune bool, restclient *restclient.Config, clientset *kubernetes.Clientset, config cfg.Config) error {
	var err error

	minikubeIp, err := unix.GetMinikubeIp(config.DataCenter.Name)
	if err != nil {
		return err
	}
	minikubeRoute, err := unix.GetIpRoute(minikubeIp)
	if err != nil {
		return err
	}

	// load custom coredns config
	var coreDnsCfgBuf strings.Builder
	data := struct {
		DataCenter     string
		MinikubeHostIp string
	}{
		config.DataCenter.Name,
		minikubeRoute.Prefsrc.String(),
	}
	err = coreDnsCfg.Execute(&coreDnsCfgBuf, data)
	if err != nil {
		return err
	}
	// TODO: don't update this every run if it hasn't changed
	_, err = clientset.CoreV1().ConfigMaps("kube-system").Update(
		context.TODO(),
		&apiv1.ConfigMap{
			TypeMeta: metav1.TypeMeta{
				Kind:       "ConfigMap",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "coredns",
				Namespace: "kube-system",
			},
			Data: map[string]string{
				"Corefile": coreDnsCfgBuf.String(),
			},
		},
		metav1.UpdateOptions{},
	)
	if err != nil {
		return err
	}

	declaredSrv, err := clientset.CoreV1().Services("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	allowedTlds := []string{"wikimedia", config.DataCenter.Name}
	reqSrvTldSet := sets.New(allowedTlds...)
	for _, node := range nodes {
		if !reqSrvTldSet.Has(node.Subdomain) {
			return fmt.Errorf("Only %v subdomains allowed, but trying to add, %v", allowedTlds, node)
		}
	}
	declaredSrvSet := sets.New[string]()
	for _, p := range declaredSrv.Items {
		declaredSrvSet.Insert(p.ObjectMeta.Name)
	}
	neededSrvSet := reqSrvTldSet.Difference(declaredSrvSet)
	for _, srv := range neededSrvSet.UnsortedList() {
		genHeadlessService(srv, clientset)
		if err != nil {
			return err
		}
	}

	declaredPods, err := clientset.CoreV1().Pods("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	declaredPodSet := sets.New[cfg.Node]()
	for _, p := range declaredPods.Items {
		declaredPodSet.Insert(
			cfg.Node{
				Name:      p.Spec.Hostname,
				Subdomain: p.Spec.Subdomain,
				Codename:  p.ObjectMeta.Labels["codename"],
			},
		)
	}

	// TODO: remove hardcoding of codenames
	puppetServer := cfg.Node{
		Name:      "puppet-server",
		Subdomain: config.DataCenter.Name,
		Codename:  "bookworm",
	}
	pkiServer := cfg.Node{
		Name:      "pki1001",
		Subdomain: config.DataCenter.Name,
		Codename:  "bullseye",
	}
	puppetDbServer := cfg.Node{
		Name:      "puppetdb1003",
		Subdomain: config.DataCenter.Name,
		Codename:  "bookworm",
	}
	reqPodSet := sets.New(puppetServer)
	reqPodSet.Insert(pkiServer)
	reqPodSet.Insert(puppetDbServer)
	for _, node := range nodes {
		reqPodSet.Insert(node)
	}
	neededPodSet := reqPodSet.Difference(declaredPodSet)
	unneededPodSet := sets.New[cfg.Node]()
	if prune {
		unneededPodSet = declaredPodSet.Difference(reqPodSet)
		for _, pod := range unneededPodSet.UnsortedList() {
			fmt.Printf("Pruning pod: %v\n", pod)
			err = delNodePod(pod, restclient, clientset, config)
			if err != nil {
				return err
			}
		}
	}
	puppetServerPod := genInfraPod(
		puppetServer.Name,
		config.Images.PuppetServer,
		puppetServer.Subdomain,
		puppetServer.Codename,
		config.Images.PullPolicy,
		allowedTlds,
	)
	pkiServerPod := genInfraPod(
		pkiServer.Name,
		config.Images.PkiServer,
		pkiServer.Subdomain,
		pkiServer.Codename,
		config.Images.PullPolicy,
		allowedTlds,
	)
	puppetDbServerPod := genInfraPod(
		puppetDbServer.Name,
		config.Images.PuppetDbServer,
		puppetDbServer.Subdomain,
		puppetDbServer.Codename,
		config.Images.PullPolicy,
		allowedTlds,
	)
	puppetServerPod.Spec.Containers[0].VolumeMounts = []apiv1.VolumeMount{
		{
			Name:      "puppet-src",
			MountPath: "/srv/puppet_code/environments/" + config.PuppetEnv,
		},
		{
			Name:      "puppet-private-src",
			MountPath: "/etc/puppet/private",
		},
	}

	puppetServerPod.Spec.Volumes = []apiv1.Volume{
		{
			Name: "puppet-src",
			VolumeSource: apiv1.VolumeSource{
				NFS: &apiv1.NFSVolumeSource{
					Server:   minikubeRoute.Prefsrc.String(),
					Path:     config.SourceCode["puppet"],
					ReadOnly: true,
				},
			},
		},
		{
			Name: "puppet-private-src",
			VolumeSource: apiv1.VolumeSource{
				NFS: &apiv1.NFSVolumeSource{
					Server:   minikubeRoute.Prefsrc.String(),
					Path:     config.SourceCode["private"],
					ReadOnly: true,
				},
			},
		},
	}

	var pods []*apiv1.Pod
	for _, pod := range neededPodSet.UnsortedList() {
		if pod.Name == puppetServer.Name {
			pods = append(pods, puppetServerPod)
		} else if pod.Name == pkiServer.Name {
			pods = append(pods, pkiServerPod)
		} else if pod.Name == puppetDbServer.Name {
			pods = append(pods, puppetDbServerPod)
		} else {
			pods = append(
				pods,
				genInfraPod(
					pod.Name,
					config.Images.PuppetClient,
					pod.Subdomain,
					pod.Codename,
					config.Images.PullPolicy,
					allowedTlds,
				),
			)
		}
	}

	for _, pod := range pods {
		fmt.Printf("Creating node pod: %s\n", podFmt(pod))
		_, err = clientset.CoreV1().Pods("default").Create(
			context.Background(),
			pod,
			metav1.CreateOptions{},
		)
		if err != nil {
			return err
		}
	}
	declaredPodSet = reqPodSet.Union(declaredPodSet.Difference(unneededPodSet))
	fmt.Println("Pods declared:")
	for _, pod := range declaredPodSet.UnsortedList() {
		fmt.Printf("\t%s\n", pod)
	}
	return nil
}

func genHeadlessService(name string, clientset *kubernetes.Clientset) error {
	var err error
	podService := &apiv1.Service{
		ObjectMeta: metav1.ObjectMeta{Name: name},
		Spec: apiv1.ServiceSpec{
			Selector: map[string]string{
				"name": name,
			},
			ClusterIP: "None",
		},
	}
	fmt.Printf("Creating headless subdomain service: %s\n", podService.ObjectMeta.Name)
	_, err = clientset.CoreV1().Services("default").Create(
		context.Background(),
		podService,
		metav1.CreateOptions{},
	)
	if err != nil {
		return err
	}
	return nil
}

func genInfraPod(name string, image string, subdomain string, codename string, pull string, tlds []string) *apiv1.Pod {
	var dnsSearches []string
	for _, tld := range tlds {
		dnsSearches = append(dnsSearches, tld+".default.svc.k8s.lan")
	}
	return &apiv1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
			Labels: map[string]string{
				"name":     subdomain,
				"codename": codename,
			},
		},
		Spec: apiv1.PodSpec{
			Hostname: name,
			// Add support for searching with ou datacenter Service subdomain
			DNSConfig: &apiv1.PodDNSConfig{
				Searches: dnsSearches,
			},
			Subdomain: subdomain,
			Containers: []apiv1.Container{
				{
					Name:            "main",
					Image:           image + ":" + codename,
					ImagePullPolicy: apiv1.PullPolicy(pull),
					Command:         []string{"/sbin/init"},
					// systemd re-execs on receiving a SIGTERM from kubernetes,
					// so we need a prestop hook to tell systemd to poweroff.
					Lifecycle: &apiv1.Lifecycle{
						PreStop: &apiv1.LifecycleHandler{
							Exec: &apiv1.ExecAction{
								Command: []string{"/bin/systemctl", "poweroff"},
							},
						},
					},
					SecurityContext: &apiv1.SecurityContext{
						Capabilities: &apiv1.Capabilities{
							Add: []apiv1.Capability{
								"SYS_CHROOT",    // SSH
								"AUDIT_WRITE",   // SSH
								"AUDIT_CONTROL", // PAM, pam_loginuid
								"NET_ADMIN",     // Ferm, aka iptables
								"NET_RAW",       // Ferm, aka iptables
								"SYS_PACCT",     // Kernel process accounting, accton
							},
						},
					},
				},
			},
		},
	}
}

// Cribbed from, https://stackoverflow.com/a/71202736
func createPodWatcher(ctx context.Context, pod string, clientset *kubernetes.Clientset) (watch.Interface, error) {
	opts := metav1.ListOptions{
		TypeMeta:      metav1.TypeMeta{},
		FieldSelector: fmt.Sprintf("metadata.name=%s", pod),
	}

	return clientset.CoreV1().Pods("default").Watch(ctx, opts)
}

func waitPodDeleted(ctx context.Context, pod string, clientset *kubernetes.Clientset) error {
	watcher, err := createPodWatcher(ctx, pod, clientset)
	if err != nil {
		return err
	}

	defer watcher.Stop()

	for {
		select {
		case event := <-watcher.ResultChan():

			if event.Type == watch.Deleted {
				fmt.Printf("Pod %s deleted\n", pod)
				return nil
			}

		case <-ctx.Done():
			fmt.Printf("Timeout waiting for deletion of pod %s\n", pod)
			return nil
		}
	}
}

func Del(nodes []cfg.Node, restclient *restclient.Config, clientset *kubernetes.Clientset, config cfg.Config) error {
	// When deleting adhoc nodes we don't have a codename, so normalize all
	// deletions by removing the codename before adding to our set.
	for i := range nodes {
		nodes[i].Codename = ""
	}
	delNodeSet := sets.New[cfg.Node]()
	for _, node := range nodes {
		delNodeSet.Insert(node)
	}

	declaredPods, err := clientset.CoreV1().Pods("default").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	declaredPodSet := sets.New[cfg.Node]()
	for _, p := range declaredPods.Items {
		declaredPodSet.Insert(
			cfg.Node{
				Name:      p.Spec.Hostname,
				Subdomain: p.Spec.Subdomain,
			},
		)
	}
	unneededPodSet := declaredPodSet.Intersection(delNodeSet)
	if unneededPodSet.Len() == 0 {
		return fmt.Errorf("Unable to delete nodes %s as they do not exist", nodes)
	}

	for _, pod := range unneededPodSet.UnsortedList() {
		fmt.Printf("Removing pod: %v\n", pod)
		err = delNodePod(pod, restclient, clientset, config)
		if err != nil {
			return err
		}
	}
	for _, pod := range unneededPodSet.UnsortedList() {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err = waitPodDeleted(ctx, pod.Name, clientset)
		if err != nil {
			return err
		}
	}
	return nil
}

func delNodePod(node cfg.Node, restclient *restclient.Config, clientset *kubernetes.Clientset, config cfg.Config) error {
	podFqdn := node.Name + "." + node.Subdomain + ".default.svc." + config.DnsDomain
	cmd := []string{
		"puppetserver",
		"ca",
		"clean",
		"--cert=" + podFqdn,
	}
	err := clientset.CoreV1().Pods("default").Delete(context.TODO(), node.Name, metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	// TODO: create helper script so we can delete certs if they exist and
	// still catch other errors. We do this after deleting the pod, so we can
	// remove resources first, if the box is overloaded.
	_ = ExecCmd(clientset, restclient, "puppet-server", cmd, os.Stdin, os.Stdout, os.Stderr)
	return nil
}

// ExecCmd exec command on specific pod and wait the command's output.
func ExecCmd(clientset *kubernetes.Clientset, config *restclient.Config, podName string,
	cmd []string, stdin io.Reader, stdout io.Writer, stderr io.Writer) error {
	req := clientset.CoreV1().RESTClient().Post().Resource("pods").Name(podName).
		Namespace("default").SubResource("exec")
	option := &apiv1.PodExecOptions{
		Command: cmd,
		Stdin:   true,
		Stdout:  true,
		Stderr:  true,
		TTY:     true,
	}
	if stdin == nil {
		option.Stdin = false
	}
	req.VersionedParams(
		option,
		scheme.ParameterCodec,
	)
	exec, err := remotecommand.NewSPDYExecutor(config, "POST", req.URL())
	if err != nil {
		return err
	}
	err = exec.Stream(remotecommand.StreamOptions{
		Stdin:  stdin,
		Stdout: stdout,
		Stderr: stderr,
	})
	if err != nil {
		return err
	}

	return nil
}

func podFmt(pod *apiv1.Pod) string {
	return fmt.Sprintf(
		"%s.%s (debian %s)",
		pod.Spec.Hostname,
		pod.Spec.Subdomain,
		pod.ObjectMeta.Labels["codename"],
	)
}
