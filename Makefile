UNAME := $(shell uname)

all: dcl

dcl:
	go build -mod vendor

install: dcl
ifeq ($(UNAME),Linux)
	mkdir -p ~/.local/bin
	cp dcl ~/.local/bin/
endif
ifeq ($(UNAME),Darwin)
	sudo cp dcl /usr/local/bin/
endif

.PHONY: darwin
darwin:
	GOOS=darwin go build -o dcl.darwin
